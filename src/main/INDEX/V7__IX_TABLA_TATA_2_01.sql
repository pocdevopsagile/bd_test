CREATE INDEX USERMDC.IX_TABLA_TATA_2_01 on USERMDC.TABLA_TATA_2(mdc_dni)
  tablespace IPC_DAT
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );