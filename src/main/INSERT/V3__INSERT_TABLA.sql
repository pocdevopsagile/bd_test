DECLARE

BEGIN

DELETE FROM USERMDC.TABLA_TATA_1;
DELETE FROM USERMDC.TABLA_TATA_2;

INSERT INTO USERMDC.TABLA_TATA_1
SELECT /*+ ALL_ROWS-HINT*/
mdc_billcycle,      
mdc_prgcode,        
mdc_customer_id,    
mdc_co_id,          
mdc_sncode,        
mdc_valid_from_date, 
mdc_status,         
mdc_prm_value_id,   
mdc_spcode,         
mdc_des,            
mdc_external_code,  
mdc_prm_description
FROM USERMDC.TABLA_EXISTENTE_1
WHERE MDC_STATUS = 'A';

INSERT INTO USERMDC.TABLA_TATA_2
SELECT /*+ ALL_ROWS-HINT*/
mdc_dni,         
mdc_fech_pago,   
mdc_bonon_nrosec,
mdc_cod_id_b7,   
mdc_status,      
mdc_sncode      
FROM USERMDC.TABLA_EXISTENTE_2
WHERE MDC_BONON_NROSEC >=200;

COMMIT;

END;
/