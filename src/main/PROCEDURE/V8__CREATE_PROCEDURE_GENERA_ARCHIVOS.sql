CREATE OR REPLACE PROCEDURE Usermdc.GENERA_ARCHIVOS(ncursor NUMBER, fecha VARCHAR2) AS
v_ErrorCode             NUMBER;
v_ErrorText             VARCHAR2(200);

BEGIN
 ---  Cursor para prepago-- 
  DECLARE CURSOR c_Prepago IS
    select distinct MDC_DNI
    from USERMDC.TABLA_EXISTENTE_2 a
    where a.MDC_FECH_PAGO=TO_DATE(fecha,'dd/mm/yyyy')
    and MDC_STATUS='A';

         
  ---  Cursor Para CRM ---
  CURSOR c_CRM IS
      select distinct a.MDC_FECH_PAGO,a.MDC_DNI,a.MDC_SNCODE
    from USERMDC.TABLA_EXISTENTE_2 a
    where a.MDC_FECH_PAGO=TO_DATE(fecha,'dd/mm/yyyy')
    and MDC_STATUS='A';
         
 BEGIN
 --activar el buffer
 DBMS_OUTPUT.ENABLE(10000000);
 
 IF ncursor = 1 THEN
  FOR v_Registro IN c_Prepago LOOP
      DBMS_OUTPUT.PUT_LINE(v_Registro.MDC_DNI||'  '||'5392'||'    '||'0');
    END LOOP;     
 ELSIF ncursor = 2 THEN
  DBMS_OUTPUT.PUT_LINE('Fecha Activacion , MDC_DNI , MDC_SNCODE');   
  FOR v_Registro IN c_CRM LOOP
      DBMS_OUTPUT.PUT_LINE(v_Registro.MDC_FECH_PAGO||','||v_Registro.MDC_DNI||','||v_Registro.MDC_SNCODE);
    END LOOP;
  END IF;       
 EXCEPTION
    WHEN NO_DATA_FOUND THEN
        v_ErrorCode := SQLCODE;
        v_ErrorText := SUBSTR(SQLERRM,1,200);
        DBMS_OUTPUT.PUT_LINE('GENERA_ARCHIVOS '||v_ErrorCode||':'||v_ErrorText);
        WHEN OTHERS THEN
        v_ErrorCode := SQLCODE;
        v_ErrorText := SUBSTR(SQLERRM,1,200);
        DBMS_OUTPUT.PUT_LINE('GENERA_ARCHIVOS '||v_ErrorCode||':'||v_ErrorText);
   END;
END;