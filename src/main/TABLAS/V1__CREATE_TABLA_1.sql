CREATE TABLE USERMDC.TABLA_TATA_1
( 
  mdc_billcycle       VARCHAR2(2),
  mdc_prgcode         VARCHAR2(10),
  mdc_customer_id     NUMBER,
  mdc_co_id           INTEGER,
  mdc_sncode          INTEGER,
  mdc_valid_from_date DATE,
  mdc_status          VARCHAR2(1),
  mdc_prm_value_id    INTEGER,
  mdc_spcode          NUMBER,
  mdc_des             VARCHAR2(150),
  mdc_external_code   VARCHAR2(32),
  mdc_prm_description VARCHAR2(100)
)
tablespace IPC_DAT
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
)
/