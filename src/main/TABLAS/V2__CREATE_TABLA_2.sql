CREATE TABLE USERMDC.TABLA_TATA_2
( 
  mdc_dni          VARCHAR2(16),
  mdc_fech_pago    DATE,
  mdc_bonon_nrosec NUMBER,
  mdc_cod_id_b7    INTEGER,
  mdc_status       VARCHAR2(1),
  mdc_sncode       INTEGER
)
tablespace IPC_DAT
pctfree 10
initrans 1
maxtrans 255
storage
(
initial 64K
next 1M
minextents 1
maxextents unlimited
)
/

  